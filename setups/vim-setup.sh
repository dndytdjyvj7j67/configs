#/bin/sh

echo "Setting up vim config:"
cd "$( dirname $(readlink -f $0) )"
mv ../.vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
echo "Don't forget to install: the_silver_search, ack"
echo "\n"
