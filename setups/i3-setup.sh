#/bin/sh

echo "Setting up i3 config:"
cd "$( dirname $(readlink -f $0) )"
mv "../i3/config" ~/.config/i3/config
echo "Don't forget to install: flameshot, tor-browser-en, nm-applet, gnome-terminal"
echo "\n"
